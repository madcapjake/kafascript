# KafaScript

General-purpose language inspired by Modern JavaScript, CoffeeScript, LiveScript, Earl Grey, and Raku

Written with the Truffle Framework, compiled to GraalVM NativeImage binary.

## Vision

- No classes (just pure prototype goodness)
- Booleans `true` `false` plus `on` and `off` (`yes` and `no` are removed due to conflict with `no` unary operator)
- Function calls need paretheses (sad but primary use case supported by `with`)
- `with` allows you to pass one additional parameter after closing parenthesis (useful for first-class functions)
- Ranges: using `to` instead of `..`
    - in range `for` `from` `to` `til` `by`
    - `to` for inclusive, `til` for exclusive.
    - `by` optionally specifies the step value.
    - `from` clause is optional, implying from 0.
- builtin await modifiers   
    - `await all`, `await race`, `await any` (Called "junctions", can be used in other ways too)
    - `await resolved`, `await fulfilled`, `await rejected` (can be combined w/ `all`, `any`, `race`)
    - saves having to use the Promise API directly and is more descriptive/declarative
- unary operator symbols are removed (these unary functions allow paren-less application)
    - `pos`, `neg`, `inv` unary "operators" on numbers
    - `so`, `very`, `no`, `not` unary "operators" on booleans
- interesting logical operators in addition to `and`, `or` (no `&&` or `||`)
    - `xor` for exclusive or
    - `implies` is an attempt to model a material implication logical operator
    - `iff` is an attempt to model a biconditional logical operator
- currencies (prefix a number with `$`, `£`, `¥`, `¤`, `€` or any currency symbol and it will be treated as a currency literal)
- dates (temporal API)
- `@foo` === `this.foo`
- `^foo` === `this.constructor.foo`
- `it` turns the surrounding expression into a closure with one argument (`it`)
- `that` in a conditional branch represents the evaluated condition expression (captures existential `?` values too)
- `|num|` absolute value
- nondeclaring assignment `:=` (mutates existing variable)
- length star `[*]` (seems alien but looks intuitive to me)
- bang-call `f!`
- pluck `v = delete o.p` (returns value before deletion)
- create objects with `new` and clone with `copy`
- clone all slots w/o prototype `{...o}`
- set prototype of `::=`
- bind a method to an object `obj||method`
- instanceof accepts `any`, `none` and `one` junctions `o instanceof any [C, K]`
- variable interpolation `"~x"` and expressions with `"~{}"`
- number comment `42_000_ms`
- numbers with variable radix `25rCoco`
- word literal `\plus` `\+`
- getters and setters `foo.:`, `foo=:`
- character/number ranges `\A to \C` `1 til 10 by 2`
- pipe `|>` (Hack style with `it` for the prior value)
- yada yada yada `...` (also `...` still keeps its usual array/obj semantics)
- labels can be applied to control flow expressions `label~for i in is`
- sugar for lists of simple strings `<[a b c]>`
- upgraded switch `given` (`default`, `when`, `proceed`, `succeed`)
- `once` executes a block exactly once even if inside of a loop or recursive routine
- `quietly` will suppress all warnings generated in it
- `gather` and `take` a dynamic list collecting control structure
- `next` == JavaScript's `continue` (can take label names)
- `last` == JavaScript's `break` (can take label names)
- `redo` restarts current block without evaluating the condition (can take label names)
- may implement something like Raku's [Phasers](https://docs.raku.org/language/phasers)


### Function operators

| syntax    | semantics             |
| --------- | --------------------- |
| `->`      | regular               |
| `\|\|->`  | bound regular         |
| `=>`      | async                 |
| `\|\|=>`  | bound async           |
| `->>`     | generator             |
| `=>>`     | async generator       |
| `\|\|->>` | bound generator       |
| `\|\|=>>` | async bound generator |


## Getting started

Still planning syntax
